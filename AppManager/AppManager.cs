﻿#if (APP_MANAGER)

#else
    #define APP_MANAGER
    #define APP_MANAGER_V1



using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.IO;
using System;

using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Linq;


/// <summary>
/// Class to hold information on each Editor "Application".
/// </summary>
[Serializable]
public class EditorApp
{
    public delegate bool OnAppGUI_del();


    private string name;
    private string description;
    private string author;
    private string email;
    private string website;
    


    public List<EditorColorGroup> groups;
    public List<AppGUIColor> colors;
    public Action<List<string>> callback;
    public List<AppPropertyData> properties;
    public List<AppFieldData> fields;
    private static Dictionary<string, System.Type> _usedTypes = new Dictionary<string, System.Type>();
    public OnAppGUI_del OnAppGUI;

    public string Name
    {
        get { return name; }
    }

    public string Description
    {
        get { return description; }
    }

    public string Author
    {
        get { return author; }
    }

    public string Email
    {
        get { return email; }
    }

    public string Website
    {
        get { return website; }
    }

    private EditorApp()
    {

    }

    public EditorApp(string name, string description = "", string author = "", string email = "", string website = "")
    {
        this.name = name;
        this.description = description;
        this.author = author;
        this.email = email;
        this.website = website;

        this.groups = new List<EditorColorGroup>();
        this.colors = new List<AppGUIColor>();
        this.properties = new List<AppPropertyData>();
        this.fields = new List<AppFieldData>();
        this.callback = null;
    }

    public EditorApp(string name, Action<List<string>> callback, string author = "", string email = "", string website = "", string description = "")
    {
        this.name = name;
        this.description = description;
        this.author = author;
        this.email = email;
        this.website = website;
        this.groups = new List<EditorColorGroup>();
        this.colors = new List<AppGUIColor>();
        this.properties = new List<AppPropertyData>();
        this.fields = new List<AppFieldData>();
        this.callback = callback;
    }


    public int addGroup(string name, bool display)
    {
        if (containsGroup(name))
            return getGroupID(name);

        groups.Add(new EditorColorGroup(groups.Count, name, display));
        return getGroupID(name);
    }

    public int getGroupID(string name)
    {
        for (int i = 0; i < groups.Count; i++)
        {
            if (groups[i].name == name)
                return i;
        }

        return -1;
    }

    public AppGUIColor addColor(string name, Color color, string group)
    {

        int groupID = getGroupID(group);

        if (groupID == -1)
            groupID = addGroup(group, false);

        if (containsColorName(name))
        {
            AppGUIColor colorData = getColorData(name);
            return colorData;
        }
        else
        {
            AppGUIColor colorData = new AppGUIColor(name, color, groupID);
            colors.Add(colorData);
            return colorData;
        }

    }

    public AppGUIColor addColor(string name, Color color, int groupID)
    {
        if (containsColorName(name))
        {
            AppGUIColor colorData = getColorData(name);
            return colorData;
        }
        else
        {
            AppGUIColor colorData = new AppGUIColor(name, color, groupID);
            colors.Add(colorData);
            return colorData;
        }

    }

    public AppGUIColor setColor(string name, Color color, int groupID)
    {
        if (containsColorName(name))
        {
            AppGUIColor colorData = getColorData(name);
            colorData.color = color;
            colorData.group = groupID;
            return colorData;
        }
        else
        {
            AppGUIColor colorData = new AppGUIColor(name, color, groupID);
            colors.Add(colorData);
            return colorData;
        }

    }

    public bool containsGroup(string name)
    {
        for (int i = 0; i < groups.Count; i++)
        {
            if (groups[i].name == name)
                return true;
        }

        return false;
    }

    public bool containsColorName(string name)
    {
        for (int i = 0; i < colors.Count; i++)
        {
            if (colors[i].name == name)
                return true;
        }

        return false;
    }

    public AppGUIColor getColorData(string name)
    {

        for (int i = 0; i < colors.Count; i++)
        {
            if (colors[i].name == name)
                return colors[i];
        }
        return null;
    }

    public AppGUIColor getColorData(int id)
    {

        if (id < 0 || id >= colors.Count)
            return null;
        return colors[id];
    }

    public void registerField(string fieldStr)
    {
        string[] splitStr = fieldStr.Split('.');

        if (splitStr.Length < 2)
        {
            throw new System.Exception("EditorApp.registerField( StaticClassName.FieldName ) - invalid format.");
        }

        System.Type type = findType(splitStr[0]);

        if (type == null)
        {
            throw new System.Exception("EditorApp.registerField( StaticClassName.FieldName ) - invalid static class name. " + splitStr[0]);
        }

        FieldInfo field = type.GetField(splitStr[1], BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

        if (field == null)
        {
            throw new System.Exception("EditorApp.registerField( StaticClassName.FieldName ) - invalid field name. " + splitStr[1]);
        }

        AppFieldData newField = new AppFieldData(splitStr[0], splitStr[1], type);
        fields.Add(newField);
    }

    public System.Type getFieldParentType(int i)
    {
        return fields[i].classType;
    }

    public FieldInfo getField(int i)
    {
        return fields[i].fieldInfo;
    }

    public FieldInfo getField(string fieldName)
    {
        for (int i = 0; i < fields.Count; i++)
        {
            if (fields[i].fieldName == fieldName)
            {
                return getField(i);
            }
        }
        return null;
    }

    public T getFieldValue<T>(string name)
    {
        FieldInfo field = getField(name);

        if (fields == null)
            return default(T);

        return (T)field.GetValue(null);
    }

    public void registerProperty(string propertyStr)
    {
        string[] splitStr = propertyStr.Split('.');

        if (splitStr.Length < 2)
        {
            throw new System.Exception("EditorApp.registerProperty( StaticClassName.FieldName ) - invalid format.");
        }

        System.Type type = findType(splitStr[0]);

        if (type == null)
        {
            throw new System.Exception("EditorApp.registerProperty( StaticClassName.FieldName ) - invalid static class name.");
        }

        PropertyInfo property = type.GetProperty(splitStr[1], BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

        if (property == null)
        {
            throw new System.Exception("EditorApp.registerProperty( StaticClassName.FieldName ) - invalid property name. " + splitStr[1]);
        }

        AppPropertyData newProperty = new AppPropertyData(splitStr[0], splitStr[1], type);

        properties.Add(newProperty);
    }

    public System.Type getPropertyParentType(int i)
    {
        return properties[i].classType;
    }

    public PropertyInfo getProperty(int i)
    {
        return properties[i].propertyInfo;
    }

    public PropertyInfo getProperty(string propertyName)
    {
        for (int i = 0; i < properties.Count; i++)
        {
            if (properties[i].propertyName == propertyName)
            {
                return getProperty(i);
            }
        }
        return null;
    }

    public T getPropertyValue<T>(string name)
    {
        PropertyInfo property = getProperty(name);

        if (property == null)
            return default(T);

        return (T)property.GetValue(null, null);
    }

    private static System.Type findType(string name)
    {
        if (_usedTypes.ContainsKey(name))
            return _usedTypes[name];

        foreach (Assembly assem in System.AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (System.Type t in assem.GetExportedTypes())
            {
                if (t.Name == name)
                {
                    _usedTypes.Add(name, t);
                    return t;
                }
            }
        }
        return null;
    }





    public class AppFieldData
    {
        public string fieldName;
        public string className;
        public System.Type classType;
        public FieldInfo fieldInfo;


        public AppFieldData(string className, string fieldName, System.Type classType)
        {

            this.fieldName = fieldName;
            this.className = className;
            this.classType = classType;
            fieldInfo = classType.GetField(fieldName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            if (fieldInfo == null)
            {
                throw new System.Exception("AppFieldData:Constructor - invalid field name.");
            }

            if (!fieldInfo.FieldType.IsPrimitive && !fieldInfo.FieldType.IsEnum && !(fieldInfo.FieldType != typeof(string)))
            {
                throw new System.Exception("AppFieldData:Constructor - field type can only be primitives, enums, and strings.");
            }

        }
    }

    public class AppPropertyData
    {
        public string propertyName;
        public string className;
        public System.Type classType;
        public PropertyInfo propertyInfo;


        public AppPropertyData(string className, string propertyName, System.Type classType)
        {

            this.propertyName = propertyName;
            this.className = className;
            this.classType = classType;
            propertyInfo = classType.GetProperty(propertyName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            if (propertyInfo == null)
            {
                throw new System.Exception("AppPropertyData:Constructor - invalid property name.");
            }

            if (!propertyInfo.PropertyType.IsPrimitive && !propertyInfo.PropertyType.IsEnum && !(propertyInfo.PropertyType != typeof(string)))
            {
                throw new System.Exception("AppPropertyData:Constructor - property type can only be primitives, enums, and strings.");
            }

        }
    }

    public class EditorColorGroup
    {
        public int id;
        public string name;
        public bool display;

        private EditorColorGroup()
        { }

        public EditorColorGroup(int id, string name, bool display)
        {
            this.id = id;
            this.name = name;
            this.display = display;
        }
    }

    public class AppGUIColor
    {
        public string name;
        public Color color;
        public int group;

        public AppGUIColor(string name, Color color, int group)
        {
            this.name = name;
            this.color = color;
            this.group = group;
        }
    }
}

public class AppManager : EditorWindow
{



    public delegate void AppLoadedDelegate(EditorApp app);
    public delegate void AppColorChangedDelegate(EditorApp app, List<string> changedColors);

    public static event AppLoadedDelegate onAppLoaded;
    public static event AppColorChangedDelegate onAppColorChanged;

    private const string defaultFile = "//default.amp";
    private static bool _isDirty = false;       //Keeps track of if the colors have been updated through the GUI.
    private static Vector2 scrollViewPos = Vector2.zero;
    private static Vector2 appScrollPos = Vector2.zero;
    private static Vector2 unregisteredAppScrollPos = Vector2.zero;
    private static AppManager colorPrefWindow;
    private static bool showAllEditors;
    public static Dictionary<string, EditorApp> registeredApps = new Dictionary<string, EditorApp>();

    private static string selectedApp;

    private static bool _initHasRun = false;

    public static bool IsDirty
    {
        get { return _isDirty; }
    }


    private static System.Type[] findTypesOf(Type ofType)
    {

        List<Type> typeList = new List<Type>();
        foreach (Assembly assem in System.AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (System.Type t in assem.GetExportedTypes())
            {
                if (t.IsSubclassOf(ofType))
                {
                    typeList.Add(t);
                }
            }
        }
        return typeList.ToArray();
    }

    private static System.Type findType(string name)
    {


        foreach (Assembly assem in System.AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (System.Type t in assem.GetExportedTypes())
            {
                if (t.Name == name)

                    return t;
            }
        }
    
        return null;
    }

[InitializeOnLoadMethod]
    static void initOnLoad()
    {

        if (_initHasRun == true)
            return;
        _initHasRun = true;

        EditorApp app = registerApplication("General", "Jason Penick", "StrikeBackStudios@gmail.com", "", "Best Application ever!");
        
        app.addGroup("Windows", true);
        app.addColor("Window", Color.grey, 0);
        app.addColor("Panel", new Color(.15f, .15f, .2f, 1), 0);
        app.addColor("Titlebar", Color.grey, 0);
        app.addColor("Button", Color.grey, 0);
        app.addColor("Button Text", Color.white, 0);
        app.addColor("Active Button", new Color(.3f,.3f,.3f,1), 0);
        app.addColor("Active Button Text", Color.white, 0);
        app.addColor("Label", Color.grey, 0);
        app.addColor("Label Text", Color.white, 0);
        loadDefaultColorPrefs();






        Type t = findType("Generator");
        Type[] tList = null;
        if (t != null)
            tList = findTypesOf(t);

        for (int i = 0; i < tList.Length; i++)
        {
            Debug.Log(tList[i].FullName);
        }


    }

    [MenuItem("Window/Editor App Manager")]
    static void showEditorWindow()
    {

        if (colorPrefWindow == null)
        {
            colorPrefWindow = EditorWindow.GetWindow<AppManager>();// new ShapeEditorWindow();// (ShapeEditorWindow)EditorWindow.GetWindow(typeof(ShapeEditorWindow));
                                                                    //editorWindow.Show();
        }
        else
            colorPrefWindow.Show();


    }

    public static void showApp(string appName)
    {
        if (colorPrefWindow == null)
        {
            colorPrefWindow = EditorWindow.GetWindow<AppManager>();// new ShapeEditorWindow();// (ShapeEditorWindow)EditorWindow.GetWindow(typeof(ShapeEditorWindow));
                                                                    //editorWindow.Show();
        }
        else
            colorPrefWindow.Show();

        selectedApp = appName;
    }

    public static EditorApp registerApplication(string name)
    {
        if (!_initHasRun)
            initOnLoad();

        if (registeredApps.ContainsKey(name))
            return registeredApps[name];

        EditorApp editorApp = new EditorApp(name);
        registeredApps.Add(name, editorApp);

        if (onAppLoaded != null)
            onAppLoaded(editorApp);
        return editorApp;
    }

    public static EditorApp registerApplication(string name, string author, string email, string website, string description)
    {
        if (!_initHasRun)
            initOnLoad();

        if (registeredApps.ContainsKey(name))
            return registeredApps[name];

        EditorApp editorApp = new EditorApp(name, author, email, website, description);
        registeredApps.Add(name, editorApp);

        if (onAppLoaded != null)
            onAppLoaded(editorApp);
        return editorApp;
    }

    public static EditorApp registerApplication(string name, Action<List<string>> callback)
    {
        if (!_initHasRun)
            initOnLoad();

        if (registeredApps.ContainsKey(name))
        {
            registeredApps[name].callback = callback;
            return registeredApps[name];
        }

        EditorApp editorApp = new EditorApp(name, callback);
        registeredApps.Add(name, editorApp);

        if (onAppLoaded != null)
            onAppLoaded(editorApp);
        return editorApp;
    }

    public static EditorApp registerApplication(string name, Action<List<string>> callback, string author, string email, string website, string description)
    {
        if (!_initHasRun)
            initOnLoad();

        if (registeredApps.ContainsKey(name))
        {
            registeredApps[name].callback = callback;
            return registeredApps[name];
        }

        EditorApp editorApp = new EditorApp(name, callback, author, email, website, description);
        registeredApps.Add(name, editorApp);

        if (onAppLoaded != null)
            onAppLoaded(editorApp);
        return editorApp;
    }

    public static EditorApp getApplication(string name)
    {
        if (registeredApps.ContainsKey(name))
            return registeredApps[name];

        return null;

    }

    public static void registerApplicationColor(string appName, string colorName, Color color, int groupID)
    {
        EditorApp app = getApplication(appName);

        if (app != null)
        {
            app.addColor(colorName, color, groupID);
        }
    }


    
    /* GUI FUNCTIONS*/
    public void OnGUI()
    {
        
        AppManagerGUI.DrawGUIBackground(AppManagerGUI.getColor("General", "Window"), new Rect(0, 0, position.width, position.height));
        GUILayout.Space(1);

        GUILayout.BeginHorizontal();

            AppManagerGUI.drawVerticalPanel(AppManagerGUI.getColor("General", "Window"), drawAppListGUI, GUILayout.Width(150), GUILayout.ExpandHeight(true));

        if (selectedApp != null && selectedApp != string.Empty)
        {

                AppManagerGUI.drawVerticalPanel(AppManagerGUI.getColor("General", "Window"), DisplayColorPreferences, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        }
        GUILayout.EndHorizontal();

        drawBottomToolBar();
        
    }

    public static void DisplayColorPreferences()
    {

        EditorApp app = getApplication(selectedApp);

        if (app == null)
            return;

        List<EditorApp.AppGUIColor> GUIColors = app.colors;
        List<string> updatedColorList = new List<string>();

        bool updated = false;
        Color newColor;
        AppManagerGUI.drawHorizontalPanel(() => AppManagerGUI.drawLabel(selectedApp + " Configuration", GUILayout.ExpandWidth(true)));
        string headerStr = string.Format("Name: {0}\r\nAuthor: {1}\r\nEmail: {2}\r\nWebsite: {3}\r\nDescription: {4}", app.Name, app.Author, app.Email, app.Website, app.Description);
        AppManagerGUI.drawHorizontalPanel(() => AppManagerGUI.drawLabel(headerStr, GUILayout.ExpandWidth(true)));
       

        AppManagerGUI.StartPanel(AppManagerGUI.getColor("General", "Window"), false);
        
        scrollViewPos = GUILayout.BeginScrollView(scrollViewPos);

        if (app.fields.Count > 0 || app.properties.Count > 0)
        {
            AppManagerGUI.drawHorizontalPanel(() => { AppManagerGUI.drawLabel("Settings", GUILayout.ExpandWidth(true)); });


            for (int i = 0; i < app.fields.Count; i++)
            {
                System.Type type = app.getFieldParentType(i);
                FieldInfo field = app.getField(i);
                AppManagerGUI.DrawFieldEditor(field, field.GetValue(null));
            }

            for (int i = 0; i < app.properties.Count; i++)
            {
                System.Type type = app.getPropertyParentType(i);
                PropertyInfo property = app.getProperty(i);
                AppManagerGUI.DrawPropertyEditor(property, property.GetValue(null, null));
            }

        }
        AppManagerGUI.drawHorizontalPanel(() => { AppManagerGUI.drawLabel("Colors", GUILayout.ExpandWidth(true)); });

        for (int n = 0; n < app.groups.Count; n++)
                {
                    EditorGUI.indentLevel = 0;
                    app.groups[n].display = EditorGUILayout.Foldout(app.groups[n].display, app.groups[n].name);
                    EditorGUI.indentLevel = 1;
                    if (app.groups[n].display)
                    {
                        for (int i = 0; i < GUIColors.Count; i++)
                        {
                            if (GUIColors[i].group != n)
                                continue;

                            GUILayout.BeginHorizontal();
                            newColor = EditorGUILayout.ColorField(new GUIContent(GUIColors[i].name), GUIColors[i].color, true, true, false, null);
                            if (!Color.Equals(newColor, GUIColors[i].color))
                            {
                                updated = true;
                                _isDirty = true;
                                GUIColors[i].color = newColor;
                                updatedColorList.Add(GUIColors[i].name);
                            }
                            GUILayout.EndHorizontal();
                        }
                    }
                }
        EditorGUI.indentLevel = 0;
        GUILayout.EndScrollView();
        AppManagerGUI.EndPanel(false);


        if (updated == true)
        {
            if (app.callback != null)
                app.callback.Invoke(updatedColorList);
            
            if (onAppColorChanged != null)
            {
                onAppColorChanged(app, updatedColorList);
            }
        }
        
        return;
    }

    public void drawAppListGUI()
    {
        List<Type> AppTypeList = AppFinder.findAppTypes(showAllEditors);

        AppManagerGUI.drawHorizontalPanel(() => AppManagerGUI.drawLabel("Applications", GUILayout.ExpandWidth(true)));

        // Application list
        using (new AppManagerGUI.VerticalPanel(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true)))
        {
            appScrollPos = GUILayout.BeginScrollView(appScrollPos);

            foreach (string key in registeredApps.Keys)
            {
                AppManagerGUI.drawButton(key, () => selectedApp = key, (key == selectedApp));
            }

            GUILayout.EndScrollView();
        }
        using (new AppManagerGUI.VerticalPanel(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true)))
        {
            showAllEditors = GUILayout.Toggle(showAllEditors, "Show Hidden Editors");

            unregisteredAppScrollPos = GUILayout.BeginScrollView(unregisteredAppScrollPos);
            foreach (Type ty in AppTypeList)
            {
                AppManagerGUI.drawButton(ty.Name, null);
            }
            GUILayout.EndScrollView();
        }
        

        //AppManagerGUI.EndPanel(false);
        

       

    }

    public static void drawBottomToolBar()
    {
        GUILayout.BeginHorizontal(EditorStyles.toolbar);
        if (GUILayout.Button("Save Profile", EditorStyles.toolbarButton))
        {
            string path = EditorUtility.SaveFilePanelInProject("Save Color Configuration", "EditorColors", "ecp", "Select something");
            if (path.Length != 0)
                saveColorPrefs(path);
        }
        if (GUILayout.Button("Save As Default", EditorStyles.toolbarButton))
        {
            saveDefaultColorPrefs();
        }
        if (GUILayout.Button("Load Profile", EditorStyles.toolbarButton))
        {
            string path = EditorUtility.OpenFilePanel("Load Color Profile", Application.dataPath, "ecp");
            if (path.Length != 0)
                loadColorPrefs(path);
        }
        if (GUILayout.Button("Load Default", EditorStyles.toolbarButton))
        {
            string path = EditorUtility.OpenFilePanel("Load Color Profile", Application.dataPath, "ecp");
            if (path.Length != 0)
                loadColorPrefs(path);
        }
        GUILayout.EndHorizontal();
    }

    public static void drawRegisterAppGUI()
    {

    }


    
    /* Profile FUNCTIONS */
    public static void loadDefaultColorPrefs()
    {
        if (File.Exists(Application.dataPath + "\\default.amp"))
            loadColorPrefs(Application.dataPath + "\\default.amp");
    }

    private static void saveDefaultColorPrefs()
    {
        saveColorPrefs(Application.dataPath + "\\default.amp");
    }

    public static void saveColorPrefs(string path)
    {
        if (path.Length < 1)
            return;

        StringBuilder sb = new StringBuilder();
        sb.AppendLine("AppManager");
        sb.Append(getProfileStr());
        if (path.Length != 0)
            File.WriteAllText(path, sb.ToString());
    }

    public static void loadColorPrefs(string path)
    {
        string colors = File.ReadAllText(path);

        parseProfile(colors);
        return;/*
    if (colors.IndexOf("AppManager") != 0)
        return;

    colors = colors.Replace("AppManager", "").Replace("RGBA(", "").Replace(")", "");
    string[] colorList = colors.Split( new string[] { "\r\n" }, System.StringSplitOptions.None ) ;
    for (int i = 1; i < (colorList.Length - 1); i++)
    {
        string[] colorVars = colorList[i].Split(',');
        Color nColor = new Color();
        nColor.r = float.Parse(colorVars[0]);
        nColor.g = float.Parse(colorVars[1]);
        nColor.b = float.Parse(colorVars[2]);
        nColor.a = float.Parse(colorVars[3]);

        GUIColors[i - 1].color = nColor;

    }*/
    }

    private static string getProfileStr()
    {
        StringBuilder output = new StringBuilder();

        foreach (string appName in registeredApps.Keys)
        {
            EditorApp app = registeredApps[appName];
            output.Append(getAppStr(app));
        }

        return output.ToString();
    }

    private static string getAppStr(EditorApp app)
    {
        StringBuilder output = new StringBuilder();

        output.AppendLine(genAppStr(app.Name));
        for (int n = 0; n < app.groups.Count; n++)
        {
            output.AppendLine(genAppGroupStr(app.Name, app.groups[n].name));
        }

        for (int n = 0; n < app.colors.Count; n++)
        {
            output.AppendLine(genAppColorStr(app.Name, app.colors[n].name, app.colors[n].color, app.colors[n].group));
        }
        for (int n = 0; n < app.fields.Count; n++)
        {
            output.AppendLine(genAppFieldStr(app.Name, app.fields[n], app.fields[n].fieldInfo.GetValue(null)));
        }
        for (int n = 0; n < app.properties.Count; n++)
        {
            output.AppendLine(genAppPropertyStr(app.Name, app.properties[n], app.properties[n].propertyInfo.GetValue(null,null)));
        }
        return output.ToString();
    }

    private static string genAppStr(string appName)
    {
        return string.Format("Application:{0}", appName);
    }

    private static string genAppGroupStr(string appName, string groupName)
    {
        return string.Format("ApplicationGroup:{0}:{1}", appName, groupName);
    }

    private static string genAppColorStr(string appName, string colorName, Color color, int groupID)
    {
        return string.Format("ApplicationColor:{0}:{1}:{2}:{3}", appName, colorName, color.ToString(), groupID);
    }

    private static string genAppFieldStr(string appName, EditorApp.AppFieldData fieldData, System.Object val)
    {

        return string.Format("ApplicationField:{0}:{1}:{2}:{3}", appName, fieldData.classType.FullName, fieldData.fieldName, val.ToString());
    }

    private static string genAppPropertyStr(string appName, EditorApp.AppPropertyData propertyData, System.Object val)
    {

        return string.Format("ApplicationProperty:{0}:{1}:{2}:{3}", appName, propertyData.classType.FullName, propertyData.propertyName, val.ToString());
    }

    public static void parseProfile(string profile)
    {
        if (profile.IndexOf("AppManager") != 0)
        {
            throw new System.Exception("AppManager: parseProfile - string is not a color profile.");
        }


        string[] lines = profile.Split(System.Environment.NewLine.ToCharArray());

        for (int i = 1; i < lines.Length; i++)
        {
            parseProfileLine(lines[i]);
        }
    }

    public static void parseProfileLine(string line)
    {
        string[] lineParams = line.Split(':');

        string cmd = lineParams[0];

        int numOfParams = lineParams.Length;
        EditorApp app;// = getApplication(lineParams[1]);
        string className;
        Type classType;
        string value;

        switch (cmd)
        {
            case "Application": //application definition
                registerApplication(lineParams[1]);
                break;
            case "ApplicationGroup": //application definition

                app = getApplication(lineParams[1]);
                app.addGroup(lineParams[2], false);

                break;
            case "ApplicationColor": //application definition

                app = getApplication(lineParams[1]);
                app.setColor(lineParams[2], colorFromString(lineParams[3]), int.Parse(lineParams[4]));
                break;
            case "ApplicationField":
                app = getApplication(lineParams[1]);
                className = lineParams[1];
                classType = Type.GetType(className);
                if (classType == null)
                {
                    Debug.Log("Type Not Found");
                    return;
                }
                string fieldName = lineParams[2];
                value = lineParams[3];
                FieldInfo fieldInfo = classType.GetField(fieldName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                if (fieldInfo == null)
                {
                    Debug.Log("Field Info is null");
                    return;
                }
                 
                fieldInfo.SetValue(null, value);
                break;

            case "ApplicationProperty":
                app = getApplication(lineParams[1]);
                className = lineParams[1];
                classType = Type.GetType(className);
                if (classType == null)
                {
                    return;
                }
                string propertyName = lineParams[2];
                value = lineParams[3];
                PropertyInfo propertyInfo = classType.GetProperty(propertyName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                if (propertyInfo == null)
                {

                    return;
                }

                propertyInfo.SetValue(null, value, null);
                break;

        }
    }

    private static Color colorFromString(string strColor)
    {
        strColor = strColor.Replace("RGBA(", "").Replace(")", "");
        string[] colorVars = strColor.Split(',');
        Color nColor = new Color();
        nColor.r = float.Parse(colorVars[0]);
        nColor.g = float.Parse(colorVars[1]);
        nColor.b = float.Parse(colorVars[2]);
        nColor.a = float.Parse(colorVars[3]);

        return nColor;
    }

}

public static class AppManagerGUI
{


    private static List<Color> _guiColorHistory = new List<Color>();
    private static Dictionary<string, Texture2D> bgTextures = new Dictionary<string, Texture2D>();
    private static EditorApp activeApp;

    public static void BackgroundColor(Color color)
    {
        _guiColorHistory.Add(GUI.backgroundColor);
        GUI.backgroundColor = color;

        if (_guiColorHistory.Count > 100)
        {
            throw new System.Exception("AppManagerGUI.GUIColor() - over 100 colors in the GUI Color History... You are more then likely using this wrong.");
        }
    }

    public static void ResetBackgroundColor()
    {
        if (_guiColorHistory.Count == 0)
            return;

        GUI.backgroundColor = _guiColorHistory[_guiColorHistory.Count - 1];
        _guiColorHistory.RemoveAt(_guiColorHistory.Count - 1);


    }

    public static void SetActiveApp(string name)
    {
        activeApp = AppManager.getApplication(name);
    }

    public static Color getColor(string appName, string colorName)
    {



        EditorApp app = AppManager.getApplication(appName);

        if (app != null)
        {
            EditorApp.AppGUIColor colorData = app.getColorData(colorName);
            if (colorData != null)
                return colorData.color;
        }

        throw new System.Exception("AppManager: Attempt to get color for application that doesnt exist.");
    }

    public static Color getColor(string colorName)
    {
        if (colorName.IndexOf('.') > -1)
        {
            string[] splitStr = colorName.Split('.');
            string appName = splitStr[0];
            colorName = splitStr[1];
            activeApp = AppManager.getApplication(appName);
        }

        if (activeApp == null)
        {
            throw new System.Exception("AppManager: getColor with no activeApp.");
        }

        EditorApp.AppGUIColor colorData = activeApp.getColorData(colorName);
        if (colorData != null)
            return colorData.color;


        throw new System.Exception("AppManager: Attempt to get color for application that doesnt exist.");
    }

    public static Texture2D getTexture(Color color)
    {

        string colorID = color.ToString();

        if (bgTextures.ContainsKey(colorID))
        {
            if (bgTextures[colorID] == null)
            {
                bgTextures[colorID] = new Texture2D(1, 1);
                bgTextures[colorID].SetPixel(0, 0, color);
                bgTextures[colorID].Apply();
            }
            return bgTextures[colorID];
        }

        Texture2D newTexture = new Texture2D(1, 1);
        newTexture.SetPixel(0, 0, color);
        newTexture.Apply();
        bgTextures.Add(colorID, newTexture);
        return newTexture;
    }

    public static void DrawGUIBackground(Color color, Rect position)
    {
        Texture2D colorTexture = getTexture(color);

        GUI.DrawTextureWithTexCoords(position, colorTexture, new Rect(0, 0, 1, 1));

    }

    public static GUIStyle createStyle(GUIStyle style, Color normalText)
    {

        GUIStyle newStyle = new GUIStyle(style);
        newStyle.normal.textColor = normalText;

        return newStyle;
    }

    public static GUIStyle createStyle(GUIStyle style, Color normalText, Color activeText)
    {
        GUIStyle newStyle = new GUIStyle(style);

        newStyle.normal.textColor = normalText;
        newStyle.active.textColor = activeText;

        return newStyle;
    }

    public static GUIStyle createStyle(GUIStyle style, Color normalText, Color activeText, Color hoverText)
    {
        GUIStyle newStyle = new GUIStyle(style);

        newStyle.normal.textColor = normalText;
        newStyle.active.textColor = activeText;
        newStyle.hover.textColor = hoverText;
        return newStyle;
    }

    public static GUIStyle createStyleWBG(GUIStyle style, Color normalText, Color activeText, Color normalBG, Color activeBG)
    {
        GUIStyle newStyle = new GUIStyle(style);

        newStyle.normal.textColor = normalText;
        newStyle.active.textColor = activeText;
        newStyle.normal.background = getTexture(normalBG);
        newStyle.active.background = getTexture(activeBG);

        return newStyle;
    }

    public static void DrawFieldEditor(FieldInfo field, System.Object obj)
    {
        System.Type type = field.FieldType;

        if (type == typeof(bool))
        {
            obj = EditorGUILayout.Toggle(field.Name, (bool)obj);
        }
        if (type == typeof(int))
        {
            obj = EditorGUILayout.IntField(field.Name, (int)obj);
        }
        if (type == typeof(float))
        {
            obj = EditorGUILayout.FloatField(field.Name, (float)obj);
        }

        if (type == typeof(Color) || type == typeof(Color32))
        {
            obj = EditorGUILayout.ColorField(field.Name, (Color)obj);
        }

        if (type.IsEnum)
        {
            obj = EditorGUILayout.EnumPopup(field.Name, obj as System.Enum);
        }


        field.SetValue(null, obj);


    }

    public static void DrawPropertyEditor(PropertyInfo property, System.Object obj)
    {
        System.Type type = property.PropertyType;

        if (type == typeof(bool))
        {
            obj = EditorGUILayout.Toggle(property.Name, (bool)obj);
        }
        if (type == typeof(int))
        {
            obj = EditorGUILayout.IntField(property.Name, (int)obj);
        }
        if (type == typeof(float))
        {
            obj = EditorGUILayout.FloatField(property.Name, (float)obj);
        }

        if (type == typeof(Color) || type == typeof(Color32))
        {
            obj = EditorGUILayout.ColorField(property.Name, (Color)obj);
        }

        if (type.IsEnum)
        {
            obj = EditorGUILayout.EnumPopup(property.Name, obj as System.Enum);
        }


        property.SetValue(null, obj, null);


    }


    public static void StartPanel(bool horizontal = true, params GUILayoutOption[] options)
    {
        if (horizontal)
        {
            AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Panel"));
            GUILayout.BeginHorizontal(GUI.skin.box, options);
            AppManagerGUI.ResetBackgroundColor();
        }
        else
        {
            AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Panel"));
            GUILayout.BeginVertical(GUI.skin.box, options);
            AppManagerGUI.ResetBackgroundColor();
        }
    }

    public static void StartPanel(Color color, bool horizontal = true, params GUILayoutOption[] options)
    {
        AppManagerGUI.BackgroundColor(color);
        if (horizontal)
            GUILayout.BeginHorizontal(GUI.skin.box, options);
        else
            GUILayout.BeginVertical(GUI.skin.box, options);

        AppManagerGUI.ResetBackgroundColor();
    }

    public static void EndPanel(bool horizontal = true)
    {
        if (horizontal)
            GUILayout.EndHorizontal();
        else
            GUILayout.EndVertical();
    }


    public static void drawHorizontalPanel(Action callback, params GUILayoutOption[] options)
    {
        AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Panel"));
        GUILayout.BeginHorizontal(GUI.skin.box, options);
        AppManagerGUI.ResetBackgroundColor();
        callback();
        GUILayout.EndHorizontal();
    }

    public static void drawHorizontalPanel(Action callback, GUIStyle style, params GUILayoutOption[] options)
    {
        AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Panel"));
        GUILayout.BeginHorizontal(style, options);
        AppManagerGUI.ResetBackgroundColor();
        callback();
        GUILayout.EndHorizontal();
    }

    public static void drawHorizontalPanel(Color color, Action callback, params GUILayoutOption[] options)
    {
        AppManagerGUI.BackgroundColor(color);
        GUILayout.BeginHorizontal(GUI.skin.box, options);
        AppManagerGUI.ResetBackgroundColor();
        callback();
        GUILayout.EndHorizontal();
        
    }


    public static void drawVerticalPanel(Action callback, params GUILayoutOption[] options)
    {
        AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Panel"));
        GUILayout.BeginVertical(GUI.skin.box, options);
        AppManagerGUI.ResetBackgroundColor();
        callback();
        GUILayout.EndVertical();
    }
    
    public static void drawVerticalPanel(Color color, Action callback, params GUILayoutOption[] options)
    {
        AppManagerGUI.BackgroundColor(color);
        GUILayout.BeginVertical(GUI.skin.box, options);
        AppManagerGUI.ResetBackgroundColor();
        callback();
        GUILayout.EndVertical();
        
    }

    public static void drawVerticalPanel(Color color, Action callback, GUIStyle style, params GUILayoutOption[] options)
    {
        AppManagerGUI.BackgroundColor(color);
        GUILayout.BeginVertical(style, options);
        AppManagerGUI.ResetBackgroundColor();
        callback();
        GUILayout.EndVertical();
        
    }

    public static void drawLabel(string text, params GUILayoutOption[] options)
    {
        AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Label"));
        GUIStyle style = AppManagerGUI.createStyle(EditorStyles.whiteLabel, AppManagerGUI.getColor("General", "Label Text"));
        GUILayout.Label(text, style, options);
        AppManagerGUI.ResetBackgroundColor();
    }


    public static void drawButton(string text, Action onClick, bool active = false, params GUILayoutOption[] options)
    {
        GUIStyle style = new GUIStyle(GUI.skin.button);
        if (active)
        {
            AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Active Button"));
            style.normal.textColor = AppManagerGUI.getColor("General", "Button Text");
        }
        else {
            AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Button"));
            style.normal.textColor = AppManagerGUI.getColor("General", "Active Button Text");
        }

        if (GUILayout.Button(text, style, options))
        {
            if (onClick != null)
                onClick();
        }

        AppManagerGUI.ResetBackgroundColor();

    }




    public class HorizontalPanel : IDisposable
    {
        

        public HorizontalPanel(bool horizontal = true, params GUILayoutOption[] options)
        {
            

            AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Panel"));
            GUILayout.BeginHorizontal(GUI.skin.box, options);
            AppManagerGUI.ResetBackgroundColor();

        }

        public HorizontalPanel(bool horizontal, Color color, params GUILayoutOption[] options)
        {

            AppManagerGUI.BackgroundColor(color);
            GUILayout.BeginHorizontal(GUI.skin.box, options);
            AppManagerGUI.ResetBackgroundColor();

        }

        public void Dispose()
        {
            GUILayout.EndHorizontal();
        }
    }

    public class VerticalPanel : IDisposable
    {


        public VerticalPanel( params GUILayoutOption[] options)
        {
            

            AppManagerGUI.BackgroundColor(AppManagerGUI.getColor("General", "Panel"));
            GUILayout.BeginVertical(GUI.skin.box, options);
            AppManagerGUI.ResetBackgroundColor();

        }

        public VerticalPanel(bool horizontal, Color color, params GUILayoutOption[] options)
        {

            AppManagerGUI.BackgroundColor(color);
            GUILayout.BeginVertical(GUI.skin.box, options);
            AppManagerGUI.ResetBackgroundColor();

        }

        public void Dispose()
        {
                GUILayout.EndVertical();
        }
    }
}


internal static class AppFinder
{

    public static List<Type> findAppTypes(bool showAll = false)
    {
        Type type = typeof(EditorWindow);

        if (!showAll)
            return AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p => type.IsAssignableFrom(p) && p.IsClass && !p.IsAbstract && !p.IsInterface && p.IsPublic).ToList<Type>();

        return AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(p => type.IsAssignableFrom(p) && p.IsClass && !p.IsAbstract && !p.IsInterface).ToList<Type>();
    }


}


#endif